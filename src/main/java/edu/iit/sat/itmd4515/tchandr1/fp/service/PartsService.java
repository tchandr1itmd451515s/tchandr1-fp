/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.service;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Parts;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author Thanusha
 */
@Named
@Stateless
public class PartsService extends AbstractService<Parts> {

    public PartsService() {
        super(Parts.class);
    }

    @Override
    public List<Parts> findAll() {
     return getEntityManager().createNamedQuery("Parts.findAll",Parts.class).getResultList();
    }
    
    public Parts findByUserName(String userName){
        return getEntityManager().createNamedQuery("Parts.findByUserName",Parts.class).setParameter("userName",userName).getSingleResult();
    }

   
}
