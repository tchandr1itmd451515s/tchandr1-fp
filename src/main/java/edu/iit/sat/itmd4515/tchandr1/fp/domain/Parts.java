/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.domain;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
//import sun.util.calendar.Gregorian;

/**
 *
 * @author Thanusha
 */
@Entity
//@Table(name = "ShopParts")
@NamedQueries({
    @NamedQuery(name = "Parts.findByName", query = "select p from Parts p where p.partName= :partName "),
    @NamedQuery(name = "Parts.findById", query = "select p from Parts p where p.partId= :partId"),
    @NamedQuery(name = "Parts.findAll", query = "select p from Parts p"),
    @NamedQuery(name = "Parts.findByUserName",query = "select p from Parts p where p.user.userName = :userName")

})
public class Parts{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long partId;

    private String partName;

    private Long partPrice;

    @ManyToOne
    /* @JoinTable(joinColumns = @JoinColumn(name ="parts_partId" ),
     inverseJoinColumns = @JoinColumn(name="shop_shopId"))*/
    @JoinColumn(name = "ShopPartId")
    private Shop shop;
    
       @ManyToMany
        @JoinTable(name = "parts_orders",
            joinColumns = @JoinColumn(name = "part_id"),
            inverseJoinColumns = @JoinColumn(name = "order_id"))
       private List<Orders> orders = new ArrayList<>();

    /**
     * Get the value of orders
     *
     * @return the value of orders
     */
    public List<Orders> getOrders() {
        return orders;
    }

    /**
     * Set the value of orders
     *
     * @param orders new value of orders
     */
    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }

    
    @OneToOne 
    @JoinColumn(name = "USERNAME")
    private User user;

    /**
     * Get the value of user
     *
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the value of user
     *
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }

    
    /**
     *
     */
    public Parts() {
    }
    

    /**
     * Get the value of shop
     *
     * @return the value of shop
     */
    public Shop getShop() {
        return shop;
    }

    /**
     * Set the value of shop
     *
     * @param shop new value of shop
     */
    public void setShop(Shop shop) {
        this.shop = shop;
    }

    /**
     *
     * @param partName
     * @param partPrice
     */
    public Parts(String partName, Long partPrice) {

        this.partName = partName;
        this.partPrice = partPrice;
    }

    /**
     *
     * @return
     */
    public Long getPartId() {
        return partId;
    }

    public void setPartId(Long partId) {
        this.partId = partId;
    }
    
    

    /**
     *
     * @return
     */
    public String getPartName() {
        return partName;
    }
    
    

    /**
     *
     * @param partName
     */
    public void setPartName(String partName) {
        this.partName = partName;
    }

    /**
     *
     * @return
     */
    public Long getPartPrice() {
        return partPrice;
    }

    /**
     *
     * @param partPrice
     */
    public void setPartPrice(Long partPrice) {
        this.partPrice = partPrice;
    }

    @Override
    public String toString() {
        return "Parts{" + "partId=" + partId + ", partName=" + partName + ", partPrice=" + partPrice + '}';
    }

}
