/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.service;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Customer;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Orders;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Parts;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Shop;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.Group;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Thanusha
 */
@Startup
@Singleton
public class DatabasePopulator {

    @EJB
    private ShopService shopService;

    @EJB
    private UserService userService;

    @EJB
    private GroupService groupService;

    @EJB
    private CustomerService customerService;

    @EJB
    private PartsService partsService;

    @EJB
    private OrdersService ordersService;

    public DatabasePopulator() {
    }

    @PostConstruct
    private void seedDatabase() {
        
         User admin = new User("admin", "admin");
         Group adminGroup = new Group("ADMIN", "Administrators Only");
         admin.addGroup(adminGroup);
          
        Group customer = new Group("CUSTOMER", "Customers Group");
        Group part = new Group("PART", "Parts Group");

        User thanu = new User("thanu", "abc123");
        User vinu = new User("vinu", "abc123");
        User regularCustomer = new User("regularCustomer", "abc123");

        thanu.addGroup(part);
        vinu.addGroup(part);
        regularCustomer.addGroup(customer);

        Shop ashop = new Shop("ASHOP");
        Shop bshop = new Shop("BSHOP");
        Shop cshop = new Shop("CSHOP");
        Shop dshop = new Shop("DSHOP");

        Orders suspension = new Orders("Bicycle Suspension");
        Orders barTape = new Orders("Bicycle barTape");
        suspension.setShop(ashop);
        ashop.getOrders().add(suspension);
        barTape.setShop(bshop);
        bshop.getOrders().add(barTape);

        Customer c1 = new Customer("Thanusha", "Chandrahasa");
        c1.setUser(regularCustomer);
        
        Customer c2 = new Customer("David","Johnson");
       // c2.setUser(regularCustomer);
        
        Customer c3 = new Customer("Henry","Jenniton");
      //  c3.setUser(regularCustomer);

        Parts p1 = new Parts("Wheels", 2L);
        p1.setUser(vinu);
        p1.setShop(ashop);
        ashop.getParts().add(p1);
        //add vinu to suspension part and suspension part to vinu
        p1.getOrders().add(suspension);
        suspension.getParts().add(p1);

        Parts p2 = new Parts("Disk", 3L);
        p2.setUser(thanu);
        p2.setShop(bshop);
        bshop.getParts().add(p2);
        //add thanu to suspension part and suspension part to thanu
        p2.getOrders().add(suspension);
        suspension.getParts().add(p2);
        //add thanu to bartape part and bartape part to thanu
        p2.getOrders().add(barTape);
        barTape.getParts().add(p2);

        groupService.create(part);
        groupService.create(customer);
        groupService.create(adminGroup);

        shopService.create(ashop);
        shopService.create(bshop);
        shopService.create(cshop);
        shopService.create(dshop);
        
        

        ordersService.create(suspension);
        ordersService.create(barTape);

        userService.create(thanu);
        userService.create(vinu);
        userService.create(regularCustomer);
        userService.create(admin);

        customerService.create(c1);
        customerService.create(c2);
        customerService.create(c3);
        partsService.create(p1);
        partsService.create(p2);
        
        //performing Update Operations
        c2.setFirstName("Stephan");
        customerService.update(c2);
        
        c3.setFirstName("Stephan");
        customerService.update(c3);
        
        //Perform ReadOperations
        List<Customer> customer1 = customerService.findAll();
        for(Customer c:customer1){
            System.out.println(c.toString());
        }
        
        //Delete operations
        customerService.remove(c3);

    }

}
