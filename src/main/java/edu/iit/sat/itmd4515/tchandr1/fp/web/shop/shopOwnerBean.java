/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.web.shop;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Parts;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Services;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Shop;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import edu.iit.sat.itmd4515.tchandr1.fp.service.PartsService;
import edu.iit.sat.itmd4515.tchandr1.fp.service.ServicesService;
import edu.iit.sat.itmd4515.tchandr1.fp.service.ShopService;
import edu.iit.sat.itmd4515.tchandr1.fp.service.UserService;
import edu.iit.sat.itmd4515.tchandr1.fp.web.AbstractJSFBean;
import edu.iit.sat.itmd4515.tchandr1.fp.web.LoginBean;
import java.security.Provider.Service;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;

/**
 *
 * @author Thanusha
 */
@Named
@RequestScoped
public class shopOwnerBean extends AbstractJSFBean {

    private static final Logger LOG = Logger.getLogger(CustomerBean.class.getName());

    private Shop shop;
    private List<Shop> shops;

    private Parts part;
    private List<Parts> parts;

    private Services service;
    private List<Services> services;
    
        private User user;
        
    private List<User> users;
    
     @EJB
    private ShopService shopService;
    @EJB
    private PartsService partsService;
    @EJB
    private ServicesService servicesService;
    @EJB
    private UserService userService;

    @Inject
    LoginBean loginBean;

    @PostConstruct
    private void postConstruct() {
        super.postContruct();

        shop = new Shop();
        part = new Parts();
        service = new Services();
        
    }

    public String executeSave() {
        if (this.shop.getShopId() != null && this.part.getPartId() != null && this.service.getServiceId() != null) {
            LOG.info("Executing update on " + this.shop.toString());
            LOG.info("Executing update on " + this.part.toString());
            LOG.info("Executing update on " + this.service.toString());
            shopService.update(shop);
            partsService.update(part);
            servicesService.update(service);
            return loginBean.getPortalPathByRole("/welcome.xhtml");
        } else {
            LOG.info("Creating " + this.shop.toString());
            LOG.info("Creating " + this.part.toString());
            LOG.info("Creating " + this.service.toString());
            shopService.create(shop, part, service);

            return loginBean.getPortalPathByRole("/welcome.xhtml");
        }
    }

    
    

    /**
     * Get the value of users
     *
     * @return the value of users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * Set the value of users
     *
     * @param users new value of users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }


    /**
     * Get the value of user
     *
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the value of user
     *
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }


   
    /**
     * Get the value of services
     *
     * @return the value of services
     */
    public List<Services> getServices() {
        return services;
    }

    /**
     * Set the value of services
     *
     * @param services new value of services
     */
    public void setServices(List<Services> services) {
        this.services = services;
    }

    /**
     * Get the value of service
     *
     * @return the value of service
     */
    public Services getService() {
        return service;
    }

    /**
     * Set the value of service
     *
     * @param service new value of service
     */
    public void setService(Services service) {
        this.service = service;
    }

    /**
     * Get the value of parts
     *
     * @return the value of parts
     */
    public List<Parts> getParts() {
        return parts;
    }

    /**
     * Set the value of parts
     *
     * @param parts new value of parts
     */
    public void setParts(List<Parts> parts) {
        this.parts = parts;
    }

    /**
     * Get the value of part
     *
     * @return the value of part
     */
    public Parts getPart() {
        return part;
    }

    /**
     * Set the value of part
     *
     * @param part new value of part
     */
    public void setPart(Parts part) {
        this.part = part;
    }

    /**
     * Get the value of shops
     *
     * @return the value of shops
     */
    public List<Shop> getShops() {
        return shops;
    }

    /**
     * Set the value of shops
     *
     * @param shops new value of shops
     */
    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }

    /**
     * Get the value of shop
     *
     * @return the value of shop
     */
    public Shop getShop() {
        return shop;
    }

    /**
     * Set the value of shop
     *
     * @param shop new value of shop
     */
    public void setShop(Shop shop) {
        this.shop = shop;
    }

}
