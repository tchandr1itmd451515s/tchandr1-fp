/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.service;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Customer;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.CustomerAddress;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author Thanusha
 */
@Named
@Stateless
public class CustomerAddressService extends AbstractService<CustomerAddress>{

       
    public CustomerAddressService() {
        super(CustomerAddress.class);
    }

 
    
    public void create(CustomerAddress customerAddress,Customer customer,User user) {
        
        
        
       getEntityManager().persist(user);
       getEntityManager().persist(customerAddress);
       getEntityManager().persist(customer);
       
    }

    public void update(CustomerAddress customerAddress) {
        getEntityManager().merge(customerAddress);
    }

    public void remove(CustomerAddress customerAddress) {
        getEntityManager().remove(customerAddress);
    }

    public CustomerAddress find(long id) {
        return getEntityManager().find(CustomerAddress.class, id);
    }

    
    
    
    public List<CustomerAddress> findAll() {
        return getEntityManager().createNamedQuery("CustomerAddress.findAll",
                CustomerAddress.class).getResultList();
    }
    
      public CustomerAddress findByUserName(String userName){
         return getEntityManager().createNamedQuery("CustomerAddress.findByUserName", CustomerAddress.class).setParameter("userName", userName).getSingleResult();
    }
   
    
}
