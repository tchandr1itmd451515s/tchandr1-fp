/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.domain;



import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.persistence.OneToOne;

/**
 *
 * @author Thanusha
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Customer.findByName", query = "select c from Customer c where c.firstName= :firstName "),

    @NamedQuery(name = "Customer.findById", query = "select c from Customer c where c.customerId= :customerId"),
    @NamedQuery(name = "Customer.findAll", query = "select c from Customer c "),
    @NamedQuery(name = "Customer.findByUserName",query = "select c from Customer c where c.user.userName= :userName")
   
})
public class Customer  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long customerId;

    private String firstName;

    private String lastName;

    @OneToOne
     @JoinColumn(name = "CustomerAddressID")
    private CustomerAddress customerAddress;
    
        
    @OneToOne
    @JoinColumn(name = "USERNAME")
    private User user;

    public Customer() {
    }

    public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    
    

    /**
     * Get the value of user
     *
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the value of user
     *
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

   

  
    
    /**
     *
     * @return
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Get the value of customerAddress
     *
     * @return the value of customerAddress
     */
    public CustomerAddress getCustomerAddress() {
        return customerAddress;
    }

    /**
     * Set the value of customerAddress
     *
     * @param customerAddress new value of customerAddress
     */
    public void setCustomerAddress(CustomerAddress customerAddress) {
        this.customerAddress = customerAddress;
    }

  

    /**
     * Get the value of lastName
     *
     * @return the value of lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the value of lastName
     *
     * @param lastName new value of lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Get the value of firstName
     *
     * @return the value of firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the value of firstName
     *
     * @param firstName new value of firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return "Customer{" + "customerId=" + customerId + ", firstName=" + firstName + ", lastName=" + lastName + '}';
    }

}
