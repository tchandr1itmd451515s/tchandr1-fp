/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.web.shop;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Parts;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import edu.iit.sat.itmd4515.tchandr1.fp.service.PartsService;
import edu.iit.sat.itmd4515.tchandr1.fp.web.AbstractJSFBean;
import edu.iit.sat.itmd4515.tchandr1.fp.web.LoginBean;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Thanusha
 */
@Named
@RequestScoped
public class PartBean extends AbstractJSFBean {

    private Parts parts;
    private static final Logger LOG = Logger.getLogger(PartBean.class.getName());

    @EJB
    PartsService partsService;
    @Inject
    LoginBean loginBean;
    
        private User user;
        
        private List<User> users;

    /**
     * Get the value of users
     *
     * @return the value of users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * Set the value of users
     *
     * @param users new value of users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }


    /**
     * Get the value of user
     *
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the value of user
     *
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }


    public PartBean() {
        super();
    }

    @PostConstruct
    protected void postContruct() {
        super.postContruct();
        parts = partsService.findByUserName(loginBean.getRemoteUser());
        LOG.info("Inside DiscJockeyBean.postConstruct() with {0}" + parts.toString());
    }

    
    public String executeUpdate(){
         LOG.info("Inside DiscJockeyBean.executeUpdate() with {0}" + parts.toString());
         partsService.update(parts);
         return loginBean.getPortalPathByRole("/welcome.xhtml");
    }
    /**
     * Get the value of parts
     *
     * @return the value of parts
     */
    public Parts getParts() {
        return parts;
    }

    /**
     * Set the value of parts
     *
     * @param parts new value of parts
     */
    public void setParts(Parts parts) {
        this.parts = parts;
    }
    
}