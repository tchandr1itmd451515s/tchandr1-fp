/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.web.shop;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Orders;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Shop;
import edu.iit.sat.itmd4515.tchandr1.fp.service.OrdersService;
import edu.iit.sat.itmd4515.tchandr1.fp.service.PartsService;
import edu.iit.sat.itmd4515.tchandr1.fp.web.AbstractJSFBean;
import edu.iit.sat.itmd4515.tchandr1.fp.web.LoginBean;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Thanusha
 */
@Named
@RequestScoped
public class OrdersBean extends AbstractJSFBean {

    private static final Logger LOG = Logger.getLogger(OrdersBean.class.getName());

    private Orders order;
    private List<Orders> orders;

    @EJB
    private OrdersService ordersService;
    @EJB
    private PartsService partsService;
    @Inject
    LoginBean loginBean;

    public OrdersBean() {
    }
    
    private void refreshShows(){
         orders = partsService.findByUserName(loginBean.getRemoteUser()).getOrders();
    }

    @PostConstruct
    private void postConstruct() {
        super.postContruct();
        order = new Orders();
        order.setShop(new Shop());
       refreshShows();
    }

    public String doDelete(Orders order) {
        LOG.info("The show to delete is " + order.toString());
        ordersService.delete(order,partsService.findByUserName(loginBean.getRemoteUser()));
        refreshShows();
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,null,"Order"+order.getOrderName()+"has been deleted"));
        return loginBean.getPortalPathByRole("/welcome.xhtml");
    }

    public String doUpdate(Orders order) {
        this.order = order;
        LOG.info("The show to update is " + order.toString());
        refreshShows();
        return "/shopPortal/order.xhtml";
    }
    
    public String executeSave() {
    if (this.order.getOrderId() != null) {
        LOG.info("Executing update on " + this.order.toString());
        ordersService.update(order);
        refreshShows();
        return loginBean.getPortalPathByRole("/welcome.xhtml");
    } else {
        LOG.info("Creating " + this.order.toString());
        ordersService.create(order, order.getShop(),partsService.findByUserName(loginBean.getRemoteUser()));
      //  ordersService.create(order);
        refreshShows();
        return loginBean.getPortalPathByRole("/welcome.xhtml");
    }
}

    /**
     * Get the value of orders
     *
     * @return the value of orders
     */
    public List<Orders> getOrders() {
        return orders;
    }

    /**
     * Set the value of orders
     *
     * @param orders new value of orders
     */
    public void setOrders(List<Orders> orders) {
        this.orders = orders;
    }

    /**
     * Get the value of order
     *
     * @return the value of order
     */
    public Orders getOrder() {
        return order;
    }

    /**
     * Set the value of order
     *
     * @param order new value of order
     */
    public void setOrder(Orders order) {
        this.order = order;
    }

}
