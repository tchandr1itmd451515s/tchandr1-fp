/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.service;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Orders;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Parts;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Shop;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Thanusha
 */
@Stateless
public class OrdersService extends AbstractService<Orders>{

    public OrdersService() {
        super(Orders.class);
    }

  

    @Override
    public List<Orders> findAll() {
         return getEntityManager().createNamedQuery("Orders.findAll",Orders.class).getResultList();
    }
    
    public void create(Orders order,Shop shop,Parts part){
        //shop is unmanaged coming from JSF.Manage it
        shop = getEntityManager().getReference(Shop.class,shop.getShopId());
       part = getEntityManager().getReference(Parts.class, part.getPartId());
        
        //and set it in the new order(manage both sides of the relationship)
        order.setShop(shop);
       shop.getOrders().add(order);
        
        //manage the association with part
        part.getOrders().add(order);
        order.getParts().add(part);
        
        getEntityManager().persist(order);
    }
    
    public void delete(Orders order,Parts part){
        //the parameters are arriving  into this method are NOT managed
        order = getEntityManager().getReference(Orders.class,order.getOrderId());
        part = getEntityManager().getReference(Parts.class, part.getPartId());
        
        //now order and part are managed
        part.getOrders().remove(order);
        order.getParts().remove(part);
        
        
        if(order.getParts().isEmpty()){
            //if we are completely deleting the order, then disassociate the shop as well
            order.getShop().getOrders().remove(order);
            order.setShop(null);
            
            getEntityManager().remove(order);
        }
    }
    
}
