/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.service;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Services;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author Thanusha
 */
@Named
@Stateless
public class ServicesService extends AbstractService<Services>{

    public ServicesService() {
        super(Services.class);
    }

  
    @Override
    public List<Services> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
       public Services findByUserName(String userName){
        return getEntityManager().createNamedQuery("Parts.findByUserName",Services.class).setParameter("userName",userName).getSingleResult();
    }
}
