/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.service;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.Group;
import java.util.List;
import javax.ejb.Stateless;

/**
 *
 * @author Thanusha
 */
@Stateless
public class GroupService extends AbstractService<Group> {

    public GroupService() {
        super(Group.class);
    }

    @Override
    public List<Group> findAll() {
        return getEntityManager().createNamedQuery("Group.findAll",Group.class).getResultList();
    }
    
       public Group findByGroupname(String groupName){
           
         return getEntityManager().createNamedQuery("Group.findByGroupname", Group.class).setParameter("groupName", groupName).getSingleResult();
    }
   
    
}
