/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.web.shop;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Customer;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.CustomerAddress;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.Group;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import edu.iit.sat.itmd4515.tchandr1.fp.service.CustomerAddressService;
import edu.iit.sat.itmd4515.tchandr1.fp.service.CustomerService;
import edu.iit.sat.itmd4515.tchandr1.fp.service.GroupService;
import edu.iit.sat.itmd4515.tchandr1.fp.service.UserService;
import edu.iit.sat.itmd4515.tchandr1.fp.web.AbstractJSFBean;
import edu.iit.sat.itmd4515.tchandr1.fp.web.LoginBean;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Thanusha
 */
@Named
@RequestScoped
public class CustomerBean extends AbstractJSFBean {
    
    @EJB
    private GroupService groupService;
    
    private static final Logger LOG = Logger.getLogger(CustomerBean.class.getName());
    
    private CustomerAddress customerAddress;
    
    private List<CustomerAddress> customerAddresses;
    
    private Customer customer;
    
    private List<Customer> customers;
    
    private User user;
    
    private List<User> users;
    
    private Group group;

    /**
     * Get the value of group
     *
     * @return the value of group
     */
    public Group getGroup() {
        return group;
    }

    /**
     * Set the value of group
     *
     * @param group new value of group
     */
    public void setGroup(Group group) {
        this.group = group;
    }
    
    @EJB
    private CustomerAddressService customerAddressService;
    
    @EJB
    private CustomerService customerService;
    
    @EJB
    private UserService userService;
    
    @Inject
    LoginBean loginBean;
    
    public void refreshShows() {
        customers = customerService.findAll();
    }

    public void refreshShowsUpdate() {
        customers = customerService.findAll();
        customerAddresses = customerAddressService.findAll();
        users = userService.findAll();
     //  user =  customerAddressService.findByUserName(loginBean.getRemoteUser()).getUser();
        //  customer = customerService.findByUserName(loginBean.getRemoteUser()).g
        //  orders = partsService.findByUserName(loginBean.getRemoteUser()).getOrders();
    }
    
    @PostConstruct
    private void postConstruct() {
        super.postContruct();
        
        customerAddress = new CustomerAddress();
        customer = new Customer();
        customer.setUser(new User());
        user = new User();
        refreshShows();
        
    }
    
    public Customer findCustomer() {
        return this.customer = customerService.findByUserName(loginBean.getRemoteUser());
    }

    public String doDelete(Customer customer) {
        LOG.info("The show to delete is " + customer.toString());
        // customerService.delete(customer, customerAddressService.findByUserName(loginBean.getRemoteUser()),userService.findByUserName(loginBean.getRemoteUser()));      
        //customerService.delete(customer);
       
          customerService.delete(customer,customer.getCustomerAddress(),customer.getUser());
        refreshShows();
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Customer" + customer.getFirstName() + customer.getLastName() + "has been deleted"));
        return loginBean.getPortalPathByRole("/welcome.xhtml");
    }
    
    public String doUpdate(Customer customer) {
        this.customer = customer;
        this.customerAddress = customerAddressService.find(customer.getCustomerAddress().getCustomerId());
        this.user = userService.findByUserName(customer.getUser().getUserName());
        LOG.info("The show to update is " + customer.toString());
        refreshShowsUpdate();
        return "/customerPortal/cust.xhtml";
    }
    
    public String executeSave() {
        if (this.customerAddress.getCustomerId() != null && this.customer.getCustomerId() != null) {
            LOG.info("Executing update on " + this.customerAddress.toString());
            LOG.info("Executing update on " + this.customer.toString());
            LOG.info("Executing update on " + this.user.toString());
            List<Customer> clist = customerService.findAll();
            List<CustomerAddress> calist = customerAddressService.findAll();
            
            CustomerAddress catemp = null;
            for (CustomerAddress ca : calist) {
                if (ca.getCustomerId().equals(this.customerAddress.getCustomerId())) {
                    catemp = ca;
                    break;
                }
                
            }
            
            catemp.setCustomerAddress1(this.customerAddress.getCustomerAddress1());
            catemp.setCustomerAddress2(this.customerAddress.getCustomerAddress2());
            catemp.setCity(this.customerAddress.getCity());
            catemp.setState(this.customerAddress.getState());
            catemp.setCountry(this.customerAddress.getCountry());
            
            Customer ctemp = null;
            for (Customer c : clist) {
                if (c.getCustomerId().equals(this.customer.getCustomerId())) {
                    ctemp = c;
                    break;
                }
            }
            
            ctemp.setFirstName(this.customer.getFirstName());
            ctemp.setLastName(this.customer.getLastName());
            ctemp.setCustomerAddress(catemp);

            //userService.update(this.user);
            //this.customerAddress.setUser(this.user);
            //customerAddressService.update(this.customerAddress);
            //this.customer.setUser(this.user);
            //this.customer.setCustomerAddress(this.customerAddress);
            customerAddressService.update(catemp);
            customerService.update(ctemp);
            refreshShows();
            return loginBean.getPortalPathByRole("/welcome.xhtml");
        } else {
            LOG.info("Creating " + this.customerAddress.toString());
            LOG.info("Creating " + this.customer.toString());
            LOG.info("Creating " + this.user.toString());
            
            Group g = groupService.findByGroupname("CUSTOMER");
            user.addGroup(g);
            customer.setUser(user);
            customerAddress.setUser(user);
            customer.setCustomerAddress(customerAddress);
            customerAddressService.create(customerAddress, customer, user);
            
            return loginBean.getPortalPathByRole("/welcome.xhtml");
        }
    }

    /**
     * Get the value of users
     *
     * @return the value of users
     */
    public List<User> getUsers() {
        return users;
    }

    /**
     * Set the value of users
     *
     * @param users new value of users
     */
    public void setUsers(List<User> users) {
        this.users = users;
    }

    /**
     * Get the value of user
     *
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the value of user
     *
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get the value of customers
     *
     * @return the value of customers
     */
    public List<Customer> getCustomers() {
        return customers;
    }

    /**
     * Set the value of customers
     *
     * @param customers new value of customers
     */
    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    /**
     * Get the value of customer
     *
     * @return the value of customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Set the value of customer
     *
     * @param customer new value of customer
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Get the value of customerAddresses
     *
     * @return the value of customerAddresses
     */
    public List<CustomerAddress> getCustomerAddresses() {
        return customerAddresses;
    }

    /**
     * Set the value of customerAddresses
     *
     * @param customerAddresses new value of customerAddresses
     */
    public void setCustomerAddresses(List<CustomerAddress> customerAddresses) {
        this.customerAddresses = customerAddresses;
    }

    /**
     * Get the value of customerAddress
     *
     * @return the value of customerAddress
     */
    public CustomerAddress getCustomerAddress() {
        return customerAddress;
    }

    /**
     * Set the value of customerAddress
     *
     * @param customerAddress new value of customerAddress
     */
    public void setCustomerAddress(CustomerAddress customerAddress) {
        this.customerAddress = customerAddress;
    }
    
}
