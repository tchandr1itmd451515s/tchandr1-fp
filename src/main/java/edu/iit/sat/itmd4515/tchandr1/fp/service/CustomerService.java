/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.service;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Customer;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.CustomerAddress;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Thanusha
 */
@Stateless
public class CustomerService extends AbstractService<Customer>{
     @PersistenceContext(unitName = "tchandr1PU")
    private EntityManager em;

    public CustomerService() {
        super(Customer.class);
    }

    

     @Override
    public void create(Customer customer) {
       getEntityManager().persist(customer);
    }

    
    public void update(Customer c) {
        em.merge(c);
    }

    public void remove(Customer c) {
        em.remove(c);
    }
    
    public void delete(Customer customer,CustomerAddress customerAddress,User user){
       customer = getEntityManager().getReference(Customer.class, customer.getCustomerId());
        customerAddress = getEntityManager().getReference(CustomerAddress.class, customerAddress.getCustomerId());
        user = getEntityManager().getReference(User.class, user.getUserName());
        getEntityManager().remove(user);
        getEntityManager().remove(customer);
        getEntityManager().remove(customerAddress);
        
       
                
    }

    public Customer find(long id) {
        return em.find(Customer.class, id);
    }

    public List<Customer> findAll() {
        return em.createNamedQuery("Customer.findAll",
                Customer.class).getResultList();
    }

    
    public Customer findByUserName(String userName){
         return em.createNamedQuery("Customer.findByUserName", Customer.class).setParameter("userName", userName).getSingleResult();
    }
   
    
}
