/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.domain;

//import one.to.many.uni.*;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
//import one.to.one.bi.Shop;

//import one.to.one.bi.Shop;
/**
 *
 * @author Thanusha
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "ShopAddress.findByName", query = "select sa from ShopAddress sa where sa.shopAddress= :shopAddress "),
    @NamedQuery(name = "ShopAddress.findById", query = "select sa from ShopAddress sa where sa.shopId= :shopId"),
    @NamedQuery(name = "ShopAddress.findAll", query = "select sa from ShopAddress sa")
})
public class ShopAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long shopId;
    private String shopAddress;

    /*@OneToOne
     private Shop shop;*/

    /**
     *
     */
    
    public ShopAddress() {
    }

    /**
     *
     * @param shopId
     * @param shopAddress
     */
    public ShopAddress(Long shopId, String shopAddress) {
        this.shopId = shopId;
        this.shopAddress = shopAddress;
    }

    /**
     *
     * @param shopAddress
     */
    public ShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    /**
     *
     * @return
     */
    public Long getShopId() {
        return shopId;
    }

    /**
     *
     * @param shopId
     */
    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    /**
     *
     * @return
     */
    public String getShopAddress() {
        return shopAddress;
    }

    /**
     *
     * @param shopAddress
     */
    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

}
