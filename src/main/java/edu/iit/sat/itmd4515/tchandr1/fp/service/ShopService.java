/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.service;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Parts;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Services;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Shop;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author Thanusha
 */
@Named
@Stateless
public class ShopService extends AbstractService<Shop>{

    public ShopService() {
        super(Shop.class);
    }
    
     public void create(Shop shop,Parts part,Services service ) {
       shop = getEntityManager().getReference(Shop.class,shop.getShopId());
       part = getEntityManager().getReference(Parts.class, part.getPartId());
       service = getEntityManager().getReference(Services.class, service.getServiceId());
       
      
       
             
       getEntityManager().persist(shop);
       getEntityManager().persist(part);
        getEntityManager().persist(service);
       
    }

    @Override
    public List<Shop> findAll() {
       return getEntityManager().createNamedQuery("Shop.findAll").getResultList();
    
    }

   
    
}
