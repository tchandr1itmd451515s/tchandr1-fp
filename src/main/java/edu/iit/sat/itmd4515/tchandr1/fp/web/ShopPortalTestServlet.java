/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.web;

import edu.iit.sat.itmd4515.tchandr1.fp.domain.Orders;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.Parts;
import edu.iit.sat.itmd4515.tchandr1.fp.service.PartsService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Thanusha
 */
@WebServlet(name = "ShopPortalTestServlet", urlPatterns = {"/shopPortal", "/shopPortal/"})
public class ShopPortalTestServlet extends HttpServlet {

    @EJB
    private PartsService partsService;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ShopPortalTestServlet</title>");
            out.println("</head>");
            out.println("<body background =" + request.getContextPath() + "/img/bg2.png" + ">");
            out.println("<h1>Servlet ShopPortalTestServlet at " + request.getContextPath() + "</h1>");
            out.println("<h2>Only shop owners can login</h2>");
            out.println("<h2>Show Owner is " + request.getRemoteUser() + "</h2>");

            if (request.isUserInRole("part")) {
                Parts p = partsService.findByUserName(request.getRemoteUser());
                out.println("<ul>");
                out.println("<li>Shop Name is " + p.getShop().getShopName() + "</li>");
                out.println("<li>This shop has <b> " + p.getPartName() + "</b>whose price is " + p.getPartPrice() + "</li>");

                for (Orders or : p.getOrders()) {

                    out.println("<li>Placed by an order of <b>" + or.getOrderName() + "</b>from" + p.getShop().getShopName() + "</li>");
                    // if more than one part
                    if (or.getParts().size() > 1) {
                        out.println("This shop also has  <ul>");
                        for (Parts p1 : or.getParts()) {
                            //if not logged in user
                            if (p1.getPartId() != p.getPartId()) {
                                out.println("<li>Part " + p1.getPartName() + " whose price is " + p1.getPartPrice() + "</li>");
                            }

                        }
                        out.println("</ul></li>");
                    } else {
                        out.println("</li>");
                    }
                }
                out.println("</ul></li>");
                out.println("</ul>");
            }

            // out.println("<h7>"+request.getContextPath()+"</h7>");
            out.println("<a href=\"" + request.getContextPath() + "/logout\">Logout</a>");

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
