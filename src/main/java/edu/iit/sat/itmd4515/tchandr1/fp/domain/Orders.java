/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.domain;

//import one.to.many.bi.*;
//import one.to.many.uni.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
//import one.to.one.bi.Shop;

//import one.to.one.bi.Shop;

/**
 *
 * @author Thanusha
 */
@Entity
@NamedQueries({
  @NamedQuery(name="Orders.findByName" ,query="select o from Orders o where o.orderName= :orderName "),
  @NamedQuery(name="Orders.findById" ,query="select o from Orders o where o.orderId= :orderId"),
  @NamedQuery(name = "Orders.findAll", query = "select o from Orders o")   
})
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderId;

    private String orderName;

  @ManyToOne
  @JoinTable(joinColumns = @JoinColumn(name ="orders_orderId" ),
            inverseJoinColumns = @JoinColumn(name="shop_shopId"))
 //  @JoinColumn(name= "ShopOrderId")
   private Shop shop;
  
    @ManyToMany(mappedBy = "orders", cascade = CascadeType.PERSIST)
    private List<Parts> parts =  new ArrayList<>();

    /**
     * Get the value of parts
     *
     * @return the value of parts
     */
    public List<Parts> getParts() {
        return parts;
    }

    /**
     * Set the value of parts
     *
     * @param parts new value of parts
     */
    public void setParts(List<Parts> parts) {
        this.parts = parts;
    }

  
  
/*  @ManyToMany(mappedBy = "orders")
  private Parts part;*/

    /**
     * Get the value of part
     *
     * @return the value of part
     */
  /*  public Parts getPart() {
        return part;
    }*/

    /**
     * Get the value of part
     */
    public Orders() {
    }

    /**
     *
     * @param orderName
     */
    public Orders(String orderName) {
        this.orderName = orderName;
    }

    /**
     *
     * @return
     */
    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    
    /**
     *
     * @return
     */
    public String getOrderName() {
        return orderName;
    }

    /**
     *
     * @param orderName
     */
    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    /**
     *
     * @return
     */
    public Shop getShop() {
        return shop;
    }

    /**
     *
     * @param shop
     */
    public void setShop(Shop shop) {
        this.shop = shop;
    }

   
    


}
