/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.domain;

//import one.to.one.uni.*;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author Thanusha
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "CustomerAddress.findByName", query = "select c from CustomerAddress c where c.customerAddress1= :customerAddress1 "),

    @NamedQuery(name = "CustomerAddress.findById", query = "select c from CustomerAddress c where c.customerId= :customerId"),
    @NamedQuery(name = "CustomerAddress.findAll", query = "select c from CustomerAddress c"),
     @NamedQuery(name = "CustomerAddress.findByUserName",query = "select c from CustomerAddress c where c.user.userName= :userName")
})

public class CustomerAddress  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long customerId;

    private String customerAddress1;

    private String CustomerAddress2;

    private String city;

    private String state;

    private String country;
    
     @OneToOne
    @JoinColumn(name = "USERNAME")   
    private User user;

    /**
     * Get the value of user
     *
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the value of user
     *
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }


    /**
     *
     */
    public CustomerAddress() {
    }

    /**
     *
     * @param customerAddress1
     * @param CustomerAddress2
     * @param city
     * @param state
     * @param country
     */
    public CustomerAddress(String customerAddress1, String CustomerAddress2, String city, String state, String country) {
        this.customerAddress1 = customerAddress1;
        this.CustomerAddress2 = CustomerAddress2;
        this.city = city;
        this.state = state;
        this.country = country;
    }

    /**
     * Get the value of country
     *
     * @return the value of country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Set the value of country
     *
     * @param country new value of country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Get the value of state
     *
     * @return the value of state
     */
    public String getState() {
        return state;
    }

    /**
     * Set the value of state
     *
     * @param state new value of state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Get the value of city
     *
     * @return the value of city
     */
    public String getCity() {
        return city;
    }

    /**
     * Set the value of city
     *
     * @param city new value of city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Get the value of CustomerAddress2
     *
     * @return the value of CustomerAddress2
     */
    public String getCustomerAddress2() {
        return CustomerAddress2;
    }

    /**
     * Set the value of CustomerAddress2
     *
     * @param CustomerAddress2 new value of CustomerAddress2
     */
    public void setCustomerAddress2(String CustomerAddress2) {
        this.CustomerAddress2 = CustomerAddress2;
    }

    /**
     * Get the value of customerAddress1
     *
     * @return the value of customerAddress1
     */
    public String getCustomerAddress1() {
        return customerAddress1;
    }

    /**
     * Set the value of customerAddress1
     *
     * @param customerAddress1 new value of customerAddress1
     */
    public void setCustomerAddress1(String customerAddress1) {
        this.customerAddress1 = customerAddress1;
    }

    /**
     *
     * @return
     */
    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    
}
