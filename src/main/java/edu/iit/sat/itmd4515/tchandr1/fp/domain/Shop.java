/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.domain;

//import one.to.one.bi.*;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import javax.persistence.OneToOne;
//import many.to.many.bi.Services;

//import one.to.many.bi.Orders;
//import one.to.many.uni.ShopAddress;
/**
 *
 * @author Thanusha
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Shop.findByName", query = "select s from Shop s where s.shopName= :shopName "),
    @NamedQuery(name = "Shop.findById", query = "select s from Shop s where s.shopId= :shopId"),
    @NamedQuery(name = "Shop.findAll", query = "select s from Shop s")
})
public class Shop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long shopId;

    @OneToMany(mappedBy = "shop")
    /*  @JoinTable(joinColumns = @JoinColumn(name = "shop_shopId"),
     inverseJoinColumns = @JoinColumn(name = "Part_partId"))*/
    private List<Parts> parts = new ArrayList<>();

    @OneToMany(mappedBy = "shop")
    private List<Orders> orders = new ArrayList<>();

    @ManyToMany
    // @JoinTable(name = "ShopServices",
    // joinColumns = @JoinColumn(name="shop_id"),
    //  inverseJoinColumns = @JoinColumn(name="service_id"))
    private List<Services> services = new ArrayList<>();
   
    private String shopName;

    @OneToOne
    private ShopAddress shopaddress;

    public Shop() {
    }

    public Shop(String shopName) {
        this.shopName = shopName;
    }
    
    


    /**
     * Get the value of shopaddress
     *
     * @return the value of shopaddress
     */
    public ShopAddress getShopaddress() {
        return shopaddress;
    }

    /**
     * Set the value of shopaddress
     *
     * @param shopaddress new value of shopaddress
     */
    public void setShopaddress(ShopAddress shopaddress) {
        this.shopaddress = shopaddress;
    }

   // @JoinTable(joinColumns = @JoinColumn(name = "shop_shopId"),
    //   inverseJoinColumns = @JoinColumn(name = "ShopAddress_shopId"))
    /**
     *
     * @return
     */
    public List<Parts> getParts() {
        return parts;
    }

    /**
     *
     * @return
     */
    public List<Orders> getOrders() {
        return orders;
    }

    /**
     *
     * @param s
     */
    public void addService(Services s) {
        if (!services.contains(s)) {
            services.add(s);
        }
        if (!s.getShops().contains(this)) {
            s.getShops().add(this);
        }
    }

    /**
     * Get the value of services
     *
     * @return the value of services
     */
    public List<Services> getServices() {
        return services;
    }

    /**
     * Set the value of services
     *
     * @param services new value of services
     */
    public void setServices(List<Services> services) {
        this.services = services;
    }

    /* public void addAddress(ShopAddress sa) {
     if (!shopAddresses.contains(sa)) {
     shopAddresses.add(sa);
     }
     }*/
    /**
     *
     * @return
     */
    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    
    /**
     *
     * @return
     */
    public String getShopName() {
        return shopName;
    }

    /**
     *
     * @param shopName
     */
    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    /*   public ShopIdentity getShopIdentity() {
     return shopIdentity;
     }

     public void setShopIdentity(ShopIdentity shopIdentity) {
     this.shopIdentity = shopIdentity;
     }*/

    /*  @Override
     public String toString() {
     StringBuilder sb = new StringBuilder(toString());
     sb.append("\n\t");
     sb.append("Shop{" + "shopId=").append(shopId).append(", shopName=").append(shopName).append(", shopIdentity=").append(shopIdentity).append(", shopAddresses=").append(shopAddresses).append(", parts=").append(parts).append(", orders=").append(orders).append(", services=").append(services).append('}');
   
     return sb.toString();
        
        
     }*/
    @Override
    public String toString() {
        return "Shop{" + "shopId=" + shopId + ", shopName=" + shopName + ", shopaddress=" + shopaddress + '}';
    }

}
