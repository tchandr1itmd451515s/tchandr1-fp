/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.tchandr1.fp.domain;

//import many.to.many.bi.*;
//import many.to.many.bi.*;
import edu.iit.sat.itmd4515.tchandr1.fp.domain.security.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
//import one.to.many.bi.*;
//import one.to.many.uni.*;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Thanusha
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Services.findByName", query = "select s from Services s where s.serviceName= :serviceName "),
    @NamedQuery(name = "Services.findById", query = "select s from Services s where s.serviceId= :serviceId"),
    @NamedQuery(name = "Services.findAll", query = "select s from Services s"),
    @NamedQuery(name = "Services.findByUserName",query = "select p from Parts p where p.user.userName = :userName")

})
public class Services{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long serviceId;

    private String serviceName;

    @Column(name = "serviceTime", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private GregorianCalendar serviceProvidedTime = new GregorianCalendar();

    @ManyToMany(mappedBy = "services")
    private List<Shop> shops = new ArrayList<>();
    
     @OneToOne 
    @JoinColumn(name = "USERNAME")
        private User user;

    /**
     * Get the value of user
     *
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the value of user
     *
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }


    /**
     *
     * @return
     */
    public List<Shop> getShops() {
        return shops;
    }

    /**
     *
     * @param shops
     */
    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }

    /**
     *
     * @param s
     */
    public void addShop(Shop s) {
        if (!shops.contains(s)) {
            shops.add(s);
        }

        if (!s.getServices().contains(this)) {
            s.getServices().add(this);
        }
    }

    /**
     * Get the value of shops
     *
     *
     */
    public Services() {
    }

    public Services(String serviceName) {
        this.serviceName = serviceName;
    }
    
    

    /**
     *
     * @param serviceId
     * @param serviceName
     * @param serviceProvidedTime
     */
    public Services(Long serviceId, String serviceName, GregorianCalendar serviceProvidedTime) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.serviceProvidedTime = serviceProvidedTime;
    }

    /**
     *
     * @return
     */
    public Long getServiceId() {
        return serviceId;
    }

    /**
     *
     * @return
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     *
     * @param serviceName
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @Override
    public String toString() {
        return "Services{" + "serviceId=" + serviceId + ", serviceName=" + serviceName + ", serviceProvidedTime=" + getServiceProvidedTime() + '}';
    }

    /**
     * @return the serviceProvidedTime
     */
    public GregorianCalendar getServiceProvidedTime() {
        return serviceProvidedTime;
    }

    /**
     * @param serviceProvidedTime the serviceProvidedTime to set
     */
    public void setServiceProvidedTime(GregorianCalendar serviceProvidedTime) {
        this.serviceProvidedTime = serviceProvidedTime;
    }

}
